document.addEventListener("DOMContentLoaded", function() {
  window.addEventListener("scroll", function() {
    if (window.scrollY >= 20) {
      document.querySelector(".kc-main_header").classList.add("header-change");
    } else {
      document
        .querySelector(".kc-main_header")
        .classList.remove("header-change");
    }
  });
  // search

  var searchWrapper = document.querySelector(".kc-search_wrapper");
  var searchContainer = document.querySelector(".search_container");
  var toggleImage = document.getElementById("toggleImage");
  var searchInput = document.querySelector(".search input");

  function openSearchContainer() {
    searchContainer.classList.add("open");
    toggleImage.src =
      "https://23438580.fs1.hubspotusercontent-na1.net/hubfs/23438580/Karix/icons8-cross-24.png";
  }

  function closeSearchContainer() {
    searchContainer.classList.remove("open");
    toggleImage.src =
      "https://23438580.fs1.hubspotusercontent-na1.net/hubfs/23438580/Karix/icons8-search%20(1).svg";
  }

  searchWrapper.addEventListener("click", function(event) {
    event.stopPropagation();
    if (searchContainer.classList.contains("open")) {
      closeSearchContainer();
    } else {
      openSearchContainer();
    }
  });

  searchContainer.addEventListener("click", function(event) {
    event.stopPropagation();
  });

  searchInput.addEventListener("keydown", function(event) {
    if (event.key === "Enter") {
      event.preventDefault();
      var query = searchInput.value.trim();
      if (query) {
        var currentUrl = window.location.href.split("?")[0];
        window.location.href =
          currentUrl + "?search=" + encodeURIComponent(query);
      }
    }
  });

  document.addEventListener("click", function() {
    if (searchContainer.classList.contains("open")) {
      closeSearchContainer();
    }
  });

  var targetNode = document.body;

  var observerConfig = {
    attributes: true,
    attributeFilter: ["class", "style"],
    childList: false,
    subtree: true,
  };

  var callback = function(mutationsList, observer) {
    for (var mutation of mutationsList) {
      if (
        mutation.type === "attributes" &&
        mutation.attributeName === "style"
      ) {
        var targetElement = mutation.target;
        if (targetElement.classList.contains("hs-menu-children-wrapper")) {
          targetElement.classList.remove("open-menu-list");
          targetElement.style.removeProperty("visibility");
          targetElement.style.removeProperty("opacity");
          targetElement.style.removeProperty("display");
        }
      }
    }
  };

  var observer = new MutationObserver(callback);

  observer.observe(targetNode, observerConfig);

  var hamburger = document.querySelector(".kc-mobile-hamburger-wrappper");
  var menuContainer = document.querySelector(".mobile-menu_container");
  var closeButton = document.querySelector(".popupmenu-close");

  // Function to toggle the menu
  function toggleMenu() {
    if (menuContainer.classList.contains("show")) {
      menuContainer.classList.remove("show");
      menuContainer.classList.add("hide");
      document.body.classList.remove("no-scroll");

      setTimeout(function() {
        menuContainer.style.display = "none";
        menuContainer.classList.remove("hide");
      }, 500);
    } else {
      menuContainer.style.display = "block";
      document.body.classList.add("no-scroll");
      setTimeout(function() {
        menuContainer.classList.add("show");
      }, 15);
    }
  }

  // Event listener for the hamburger icon
  hamburger.addEventListener("click", toggleMenu);

  // Event listener for the close icon
  closeButton.addEventListener("click", toggleMenu);

  document.addEventListener("click", function(event) {
    if (
      !menuContainer.contains(event.target) &&
      !hamburger.contains(event.target)
    ) {
      if (menuContainer.classList.contains("show")) {
        toggleMenu();
      }
    }
  });
});
