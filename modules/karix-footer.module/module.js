document.addEventListener("DOMContentLoaded", function() {
  document.querySelectorAll(".karix-title_container").forEach((container) => {
    container.addEventListener("click", function() {
      const footerContainer = this.parentElement;
      const footerContent = footerContainer.querySelectorAll(
        ".menu-footer, .karix-mobile-footer-contact, .karix-social-contacts, .karix-footer-support_wrapper"
      );
      const toggleDropdownImage = footerContainer.querySelector(
        ".karix-drop-down"
      );
      footerContent.forEach((content) => {
        content.classList.toggle("mobile-visible");
        if (content.classList.contains("mobile-visible")) {
          content.classList.remove("dropdown-open");
        } else {
          content.classList.add("dropdown-open");
        }
      });
      if (toggleDropdownImage) {
        toggleDropdownImage.classList.toggle("dropdown-active");
        container.classList.toggle("karix-title-active");

      }
    });
  });

  // Initiate the footer
  siteFooter();

  // Add resize event listener
  window.addEventListener("resize", function() {
    siteFooter();
  });

  function siteFooter() {
    var siteContent = document.getElementById("main-content");
    var siteFooter = document.getElementById("gbl-footer");

    if (siteContent && siteFooter) {
      var siteContentHeight = siteContent.offsetHeight;
      var siteContentWidth = siteContent.offsetWidth;
      var siteFooterHeight = siteFooter.offsetHeight;
      var siteFooterWidth = siteFooter.offsetWidth;
      siteContent.style.marginBottom = siteFooterHeight + 30 + "px";
    }
  }
});
